const {AWS}     = require('@traibe/aws');
const SNS       = new AWS.SNS({ apiVersion: "2010-03-31" });

async function listTopics(topic){
    try{
        const topics    = await SNS.listTopics({}).promise();
        const arns      = topics.Topics.map( item => item.TopicArn);
        const filtered  = arns.filter( item => item.includes(topic) );
        if(filtered.length > 0) 
            return filtered;
    }catch(err){
        console.log(err);
    }
    return [];
}

async function subscribeToTopic(topic, endpoint){
    return new Promise(async (resolve, reject)=>{

        if(process.env.NODE_ENV !== 'production')
            return reject(`Subscription to '${topic}' permitted production mode only`);

        try{
            const arns = await listTopics(topic);
            if(!arns || arns.length <= 0)
                throw new Error(`Topic ${topic} doesnt exist`);

            //Check endpoint is ready, will throw error if not
            const response = await axios.post(endpoint, null, {headers: { "x-amz-sns-message-type": "Sandbox"}}); 
            if(!response.status === 200)
                throw new Error("Endpoint not ready, headers: 'x-amz-sns-message-type:Sandbox' is missing or endpoint not operational");

            // Ok its listening ok.
            console.log(`Subscribing to '${topic}': ${arns[0]}`)
            const params = {
                Protocol: endpoint.toLowerCase().includes('https:') ? 'HTTPS' : 'HTTP',
                Endpoint: endpoint,
                TopicArn: arns[0],
            }  

            //Now subscribe.
            SNS.subscribe(params, (err, data) =>{
                if(err) return reject(err)
                console.log(`Subscription OK: Listening on ${params.endpoint} to ${params.TopicArn}`)
                return resolve(data)
            })

        }catch(error){
            console.log(`Bypassing SNS subscription, (${error})`);
            return reject(error);
        }
    })
}

async function publishToTopic(topic){
    try{
        const topics = await listTopics(topic);
        return function(payload){
            return new Promise((resolve, reject)=>{
                if(!topics || topics.length <= 0)
                    return reject(`No topics matching ${topic}`);

                // Build the payload object
                var payloadObject = {
                    default: JSON.stringify(payload) //Default required by AWS
                };
                
                // Build the params
                var params= { 
                    TargetArn: topics[0],
                    Message: JSON.stringify(payloadObject),
                    MessageStructure: 'json' // Super important too
                };

                SNS.publish(params, (err, data) => {
                    if(err) return reject(err);
                    return resolve(data);
                })
            });
        }
    }catch(err){
        return function(payload){
            return Promise.reject(err.message);
        }
    }
}

class TopicPublisher {
    constructor(topicSearch, intervalDuration=60000){
        this.topicSearch        = topicSearch;
        this.intervalDuration   = Math.max(intervalDuration,100);
        this.topic              = null 
        this.interval           = null
        
        // Bind the methods
        this.init               = this.init.bind(this);
        this.post               = this.post.bind(this);
        this.handleSearch       = this.handleSearch.bind(this);

        // Init
        this.init();       
    }

    // Kick off the refresh process
    async init(){
        clearInterval(this.interval); //Kill if has been run before.
        let searchFun = async () => {
            try{
                console.log(`Performing Search for SNS Topic: ${this.topicSearch}, Every: ${this.intervalDuration}ms`);
                await this.handleSearch()
            }catch(err){
                console.error(err.message);
            }
            return this.topic;
        }
        this.interval = setInterval(searchFun, this.intervalDuration);
        await searchFun();
    }

    handleSearch(){
        let self = this;
        return new Promise(resolve => {
            listTopics(self.topicSearch)
                .then((topics) => {
                    if(topics.length > 1)
                        console.warn("Matches more than one topic, only the first will be selected");
                    if(topics.length == 1 && self.topic === null)
                        console.log(`Topic '${topics[0]}' resolved...`);
                    self.topic = topics[0]; // Undefined if length 0
                    resolve(self.topic)
                })
                .catch((err)=>{
                    console.error(err.message);
                    self.topic = undefined;
                    resolve(self.topic);
                })
        });
    }

    async post(payload){
        // If topic not resolved, force it to be resolved
        if(this.topic === null)
            await this.handleSearch();

        // No match, reject
        if(this.topic === undefined) 
            return Promise.reject(`No topic matching search parm: ${this.topicSearch}`);

        // Build the payload object, 'default' required by AWS
        let payloadObject       = { default: JSON.stringify(payload) }; 
        let Message             = JSON.stringify(payloadObject);
        let MessageStructure    = 'json'; // Super important too

        // Post to SNS
        return new Promise((resolve, reject) => {
            var params= { TargetArn:this.topic, Message, MessageStructure};
            SNS.publish(params, (err, data) => {
                if(err) return reject(err.message);
                resolve(data);
            });
        })
    }
};

module.exports = {
    SNS,
    listTopics,
    subscribeToTopic,
    publishToTopic,
    TopicPublisher
}